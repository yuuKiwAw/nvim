local opts = {}

return {
  on_ready = function(server)
    server:setup(opts)
  end,
}
